package le_pendu_joueur;

import java.io.Serializable;


/**
 * La classe joueur1 def l'objet joueur1
 *avec les attributs nom et score
 * @author nicolas
 *
 */
public class Joueur1 implements Joueur, Serializable {
	/**
	 * attributs de la classe Joueur1
	 */
	private static final long serialVersionUID = 1L;
	private String nom = "Joueur 1";
	private int score = 0;
	private int nbMot = 0;
	
	
	public Joueur1() {
		this.nom = "";
		this.score = 0;
		this.nbMot = 0;
	}
	
	/**
	 * Constructeur avec les parametres suivant
	 * @param pNom
	 * @param pScore
	 */
	public Joueur1(String pNom, int pScore) {
		this.nom = pNom;
		this.score = pScore;
	}
	/**
	 * Accesseurs et mutateurs : def et obtenir le nom et le score
	 */
	public String getNom() {
		return this.nom;
	}
	public int getScore() {
		return this.score;
	}
	public void setNom(String pNom) {
		this.nom = pNom;
	}
	public void setScore(int pScore) {
		this.score = pScore;
	}
	public int getNbMot() {
		return this.nbMot;
	}
	public void setNbMot(int pNbMot) {
		this.nbMot = pNbMot;
	}

}
