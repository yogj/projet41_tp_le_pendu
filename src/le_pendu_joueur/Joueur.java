package le_pendu_joueur;

import java.io.Serializable;

/**
 * interface joueur
 * @author nicolas
 *
 */
public interface Joueur extends Serializable {

	public String getNom();
	
	public int getScore();
	
	public void setScore(int pScore);
	
	public void setNom(String pNom);
	
	public void setNbMot(int pNbMot);
	
	public int getNbMot();
}
