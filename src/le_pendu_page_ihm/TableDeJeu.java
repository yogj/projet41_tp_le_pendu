package le_pendu_page_ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import le_pendu_clavier.Clavier;
import le_pendu_clavier.Observateur;
import le_pendu_joueur.Joueur1;

/**
 * classe table de jeu = moteur du jeu + fenetre de jeu
 * @author nicolas
 *
 */

public class TableDeJeu extends JPanel {
	//--Variables d'instance
	private String nom = "Table De Jeu";
	private static final long serialVersionUID = 1L;
	private File dico = new File("Ressources/Fichiers/dictionnaire.txt");
	//--Variables d'instance IHM
	private JPanel panKey = new JPanel();
	private JLabel scoreActuel, nbMotTrouveLbl,
			       motCacheLbl = new JLabel(),
			       etqImage = new JLabel(new ImageIcon("Ressources/Images/imgVierge.JPG"));
	//--Variables d'instance objet
	private Clavier clavier = new Clavier();
	private Joueur1 joueur;

	private String nomJoueur;
	private int scoreJoueur;
	private int nbMotTrouve;
	private String mot = "";
	private String motCache;
	private char[] listMotCache;//Liste de char '*' remplacee au fur et a mesure par les lettre du mot
	private char[] listLettre;//Liste de char constituee des lettres du mot
	private int erreur = -1;
	private boolean reponse;//renvoie true si la lettre est juste
	private boolean partieLancee = false;

	
	/**
	 * Constructeur sans param
	 */
	public TableDeJeu() {

		//--Initialisation du joueur via une boite de dialogue
		JOptionPane jopNom = new JOptionPane();
		this.nomJoueur = jopNom.showInputDialog(null, "Quel est votre nom ?", "Identification", JOptionPane.QUESTION_MESSAGE);
		JOptionPane jopBonjour = new JOptionPane();
		jopBonjour.showMessageDialog(null, "Bonjour "+this.nomJoueur, "Bonjour", JOptionPane.INFORMATION_MESSAGE);
		this.joueur = new Joueur1(this.nomJoueur, 0); 
		this.nbMotTrouve = this.joueur.getNbMot();
		this.scoreJoueur = this.joueur.getScore();
		
		//--Panel Nord contient les 3 etiquettes nomJoueur, nbMotTrouve, scoreActuel
		JPanel panNord = new JPanel();
		panNord.setLayout(new BoxLayout(panNord, BoxLayout.PAGE_AXIS));
		Font police = new Font("Arial", Font.BOLD, 18);
		
		//--Etiquette nomJoueur
		JLabel nomJoueurLbl = new JLabel(this.nomJoueur+" tente sa chance !");
		nomJoueurLbl.setFont(police);
		nomJoueurLbl.setPreferredSize(new Dimension(300,50));
		nomJoueurLbl.setForeground(Color.BLACK);
		nomJoueurLbl.setHorizontalAlignment(JLabel.CENTER);
		panNord.add(nomJoueurLbl);
				
		//--Etiquette nbMotTrouve
		this.nbMotTrouveLbl = new JLabel("Nombres de mots trouv�s : "+Integer.toString(this.nbMotTrouve));
		nbMotTrouveLbl.setFont(police);
		nbMotTrouveLbl.setPreferredSize(new Dimension(300,50));
		nbMotTrouveLbl.setForeground(Color.BLACK);
		nbMotTrouveLbl.setHorizontalAlignment(JLabel.CENTER);	
		panNord.add(nbMotTrouveLbl);
				
		//--Etiquette score actuel
		this.scoreActuel = new JLabel("Votre score actuel : "+Integer.toString(this.scoreJoueur));
		scoreActuel.setFont(police);
		scoreActuel.setPreferredSize(new Dimension(300,50));
		scoreActuel.setForeground(Color.BLACK);
		scoreActuel.setHorizontalAlignment(JLabel.CENTER);
		panNord.add(scoreActuel);
			
		//--Panel Sud : le mot a decouvrir
		Font policeMot = new Font("Arial", Font.BOLD, 36);
		JPanel panSud = new JPanel();
		motCacheLbl.setFont(policeMot);
		motCacheLbl.setPreferredSize(new Dimension(500,50));
		motCacheLbl.setForeground(Color.BLUE);
		motCacheLbl.setHorizontalAlignment(JLabel.CENTER);
		motCacheLbl.setBorder(BorderFactory.createRaisedBevelBorder());
		
		//--On initialise le mot a decouvrir et on affiche des etoiles ds l'etiquette 
		this.setMotCache();
		System.out.println("Le mot choisi : "+mot);//Controle
		motCacheLbl.setText(motCache);
		panSud.add(motCacheLbl);
	
		//--Panel Image qui contient l'image du pendu
		JPanel panImage = new JPanel();
		etqImage.setPreferredSize(new Dimension(350,350));
		etqImage.setHorizontalTextPosition(JLabel.CENTER);
		panImage.add(etqImage);
			
		//--Panel clavier qui contient le clavier
		panKey.setPreferredSize(new Dimension (400, 350));
		this.clavier.addObservateur(new Observateur() {
			public void update(char pLettre) {
				verifierLettre(pLettre);
			}
		});
		panKey.setBorder(BorderFactory.createLoweredBevelBorder());
		panKey.add(clavier);
		
		//--Panel central contenant le clavier et l'image
		JPanel panCentre = new JPanel();
		panCentre.setLayout(new FlowLayout());
		panCentre.add(panKey);
		panCentre.add(panImage);
				
		//--Panel contenant tous les autres
		JPanel container = new JPanel();
		container.setLayout(new BorderLayout());
		container.add(panNord,  BorderLayout.NORTH);
		container.add(panSud, BorderLayout.SOUTH);
		container.add(panCentre, BorderLayout.CENTER);
		
		this.add(container);
				
	}
	/**
	 * Methode gerant une nvelle partie
	 * On reinitialise les mots, leur affichage, le nb d'erreur, l'image
	 * et on met a jour le score et le nb de mot trouver
	 */
	public void nouvellePartie() {
		this.panKey.removeAll();
		this.partieLancee = true;
		this.clavier = new Clavier();
		this.clavier.addObservateur(new Observateur() {
			public void update(char pLettre) {
				verifierLettre(pLettre);
			}
		});
		this.motCache = "";
		this.mot = "";
		this.motCacheLbl.setText("");
		this.setMotCache();
		System.out.println("nveau mot : " + mot);
		this.erreur = -1;
		this.motCacheLbl.setText(motCache);
		this.etqImage.setIcon(new ImageIcon("Ressources/Images/imgVierge.JPG"));
		panKey.add(clavier);
	}		
	
	/**
	 * Methode def le mot cache, genere les listes de char pour afficher le mot cache par des etoiles
	 * puis reconstruit le mot � trouver sans caracteres speciaux
	 */
	public void setMotCache() {
		int nbre = (int)(Math.random()*336529);
		//--lecture du fichier dictionnaire a la ligne nbre
		try {
			LineNumberReader fnr = new LineNumberReader(new FileReader(dico));
			int carac;
			while((carac = fnr.read()) != -1){
				if(fnr.getLineNumber() == (nbre+1))
					break;
				else if(fnr.getLineNumber() == nbre)
						this.mot += (char)carac;						
			}
			this.mot = this.mot.trim().toUpperCase();
			fnr.close();
		}catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Erreur de chargement depuis le fichier de mots !", "ERREUR", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Erreur de chargement depuis le fichier de mots !", "ERREUR", JOptionPane.ERROR_MESSAGE);
		}
		//--On cree une 2 listes de char : 1 avec les lettres du mot et 1 avec une etoile pour chaque lettre du mot
		this.motCache = "";
		this.listMotCache = new char[this.mot.length()];
		this.listLettre = new char[this.mot.length()];
		for (int i = 0 ; i<this.mot.length() ; i++) {
			this.listLettre[i] = this.mot.charAt(i);
			this.listMotCache[i] = '*';
			this.motCache = motCache + this.listMotCache[i];
		}
		//--On reconstruit le mot sans carac speciaux
		this.charSpeciaux();
		this.mot ="";
		for (int j = 0; j<this.motCache.length(); j++) {
			this.mot += this.listLettre[j];
		}
		System.out.println("mot reconstruit : "+this.mot);//--Controle
	}

	/**
	 * Methode verifiant si la lettre proposee est ds le mot cache
	 * compte le nb d'erreur et gere l'affichage des images
	 * @param pLettre
	 */
	
	public void verifierLettre(char pLettre) {
		//--on verif si la lettre saisie est juste ou non et on recupere son emplacement ds la liste
		reponse = false;
		for (int i = 0 ; i<this.mot.length(); i++) {
			//--on gere les caracteres speciaux
			this.charSpeciaux();
			
			//--si la reponse est juste on devoile la lettre ds le motCache
			if(this.listLettre[i] == pLettre ) {
				this.listMotCache[i] = pLettre;//on chge l'etoile par la lettre
				reponse = true;
				}
			this.motCache = "";//on reinitialise le motcache
			for(char l : listMotCache)
				this.motCache += l;
			this.motCacheLbl.setText(motCache);//on affiche avec les etoiles et la lettre
			}
		
		//--si la reponse est fausse, on incremente le compteur d'erreur et on gere les image
		if (!reponse) {
			reponse = false;
			erreur ++;
			switch(erreur) {
				case 0: this.etqImage.setIcon(new ImageIcon("Ressources/Images/imgErreur1.JPG"));break;
				case 1: this.etqImage.setIcon(new ImageIcon("Ressources/Images/imgErreur2.JPG"));break;
				case 2: this.etqImage.setIcon(new ImageIcon("Ressources/Images/imgErreur3.JPG"));break;
				case 3: this.etqImage.setIcon(new ImageIcon("Ressources/Images/imgErreur4.JPG"));break;
				case 4: this.etqImage.setIcon(new ImageIcon("Ressources/Images/imgErreur5.JPG"));break;
				case 5: this.etqImage.setIcon(new ImageIcon("Ressources/Images/imgErreur6.JPG"));break;
				case 6: cPerdu(); //7 erreurs, c perdu
			}
		}
		else {//-- si le mot est decouvert
			System.out.println("mot cache : "+motCache+ " - mot : "+mot);
			if(motCache.equals(mot)) {
				cGagne();
			}
		}
	}
	
	/**
	 * Methode de gestion lorsque la partie est perdue : fenetre de dialogue, revele le mot, 
	 */
	
	public void cPerdu() {
		this.etqImage.setIcon(new ImageIcon("Ressources/Images/imgPendu.JPG"));
		this.motCacheLbl.setText(this.mot);//--On affiche le mot
		this.partieLancee = false;
		JOptionPane jopPerdu = new JOptionPane();
		int option = jopPerdu.showConfirmDialog(null, "Vous avez perdu !\n Voulez-vous recommencer ? ", 
						  			   "Partie perdue", JOptionPane.YES_NO_OPTION,
						  			   JOptionPane.QUESTION_MESSAGE);
		if (option == JOptionPane.OK_OPTION) {
			this.clavier.delObservateur();
			nouvellePartie();
		}
	}
	/**
	 * Methode de gestion lorsque la partie est gagnee : fenetre de dialogue, score et prop de nvelle partie
	 */
	public void cGagne() {

		JOptionPane jopGagne = new JOptionPane();
		jopGagne.showMessageDialog(null, "Bravo ! Vous avez trouv� le mot cach� !\n"
										+ " Vous avez fait "+((erreur == -1 )? "aucune erreur. Felicitation !\n" : erreur+1 +" erreurs.\n")
										+ " Votre score est : " +calcScore(),
										"Partie gagn�e", JOptionPane.INFORMATION_MESSAGE);
		
		//--On sauvegarde
		this.nbMotTrouve +=1;
		this.joueur.setScore(this.scoreJoueur);
		this.joueur.setNbMot(this.nbMotTrouve);
		this.scoreActuel.setText("Votre score actuel : "+Integer.toString(this.scoreJoueur));
		this.nbMotTrouveLbl.setText("Nombres de mots trouv�s : "+Integer.toString(this.nbMotTrouve));
		this.partieLancee = false;
		JOptionPane jopNvellePartie = new JOptionPane();
		int option = jopNvellePartie.showConfirmDialog(null, "Voulez-vous recommencer ? ", 
	  			   "Nouvelle Partie", JOptionPane.YES_NO_OPTION,
	  			   JOptionPane.QUESTION_MESSAGE);
		if (option == JOptionPane.OK_OPTION) {
			this.clavier.delObservateur();
			nouvellePartie();
		}
	}
	
	/**
	 * Methode retournant le nom de la page
	 * @return Table de jeu
	 */
	public String getNom() {
		return this.nom;
	}
	/**
	 * Methode retournant le joueur
	 * @return
	 */
	public Joueur1 getJoueur() {
		return this.joueur;
	}
	/**
	 * Methode de calcul du score
	 * @return
	 */
	public int calcScore() {
		switch(erreur) {
			case -1: this.scoreJoueur += 100;break; 
			case 0: this.scoreJoueur += 50;break;
			case 1: this.scoreJoueur += 35;break;
			case 2: this.scoreJoueur += 25;break;
			case 3: this.scoreJoueur += 15;break;
			case 4: this.scoreJoueur += 10;break;
			case 5: this.scoreJoueur += 5;break;
		}
		return this.scoreJoueur;
	}
	/**
	 * Methode de gestion des caracteres speciaux
	 */
	public void charSpeciaux() {
		for (int i = 0 ; i<this.mot.length(); i++) {
			if (this.listLettre[i] == '�'|| this.listLettre[i] =='�'|| this.listLettre[i] == '�') 
				this.listLettre[i] = 'A'; 
			else if (this.listLettre[i] == '�'|| this.listLettre[i] =='�'|| this.listLettre[i] == '�' || this.listLettre[i] == '�' )
				this.listLettre[i] = 'E';
			else if (this.listLettre[i] == '�'|| this.listLettre[i] =='�')
				this.listLettre[i] = 'I';
			else if (this.listLettre[i] == '�'|| this.listLettre[i] =='�')
				this.listLettre[i] = 'O';
			else if (this.listLettre[i] == '�'|| this.listLettre[i] =='�')
				this.listLettre[i] = 'U';
			else if (this.listLettre[i] == '�')
				this.listLettre[i] = 'C';
		}
	}
	
	public boolean getPartieLancee() {
		return this.partieLancee;
	}
	
	public void setPartieLancee(boolean pPartieLancee) {
		this.partieLancee = pPartieLancee;
	}
	
	public boolean getReponse() {
		return this.reponse;
	}
}


