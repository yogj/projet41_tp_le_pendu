package le_pendu_page_ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Comparator;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import le_pendu_joueur.Joueur1;


public class Scores extends JPanel{
	/**
	 * variables d'instance
	 */
	private static final long serialVersionUID = 1L;
	private JLabel[] listEtiquette = new JLabel[10];
	private CopyOnWriteArrayList<Joueur1> listJoueur;
	private String nom = "Scores";

	
	public Scores(CopyOnWriteArrayList<Joueur1> pListeJoueur) {
		//--on param le rangement des comp et la couleur de fond
		this.setLayout(new BorderLayout());
		this.setBackground(Color.WHITE);
		
		//--Etiquette et panneau titre
		Font policeTitre = new Font("Arial", Font.BOLD, 35);
		JLabel titre = new JLabel("PANNEAU DES SCORES");
		titre.setFont(policeTitre);
		titre.setPreferredSize(new Dimension (500,100));
		titre.setHorizontalAlignment(JLabel.CENTER);
		titre.setForeground(Color.BLACK);
		this.add(titre, BorderLayout.NORTH);
		
		//--on init la liste en recuperant la liste de notre classe LePendu
		this.listJoueur = pListeJoueur;
		this.triDecroissant();
		System.out.println("liste score param : ");//Controle
		for (Joueur1 j : pListeJoueur) {//Controle
			System.out.println("joueur param : "+j.getNom()+" - "+j.getScore());//Controle
		}
		//--On recup les differents joueurs de la listJoueur
		Font police = new Font("Arial", Font.BOLD, 18);
		JPanel panLabel = new JPanel();
		panLabel.setLayout(new BoxLayout(panLabel, BoxLayout.PAGE_AXIS));
		//--On affiche dans 10 etiquettes le nom du joueur et son score
		for(int i = 0; i < this.listJoueur.size(); i++) {
			listEtiquette[i] = new JLabel();
			listEtiquette[i].setFont(police);
			listEtiquette[i].setPreferredSize(new Dimension(400,100));
			listEtiquette[i].setForeground(Color.BLACK);
			listEtiquette[i].setText(+i + 1+". "+this.listJoueur.get(i).getNom()+" .................. "+this.listJoueur.get(i).getScore());
			System.out.println("etiquettes "+i+" joueur "+this.listJoueur.get(i).getNom()+" - score "+this.listJoueur.get(i).getScore()+"\n");//Controle
			panLabel.add(listEtiquette[i]);
		}
		this.add(panLabel, BorderLayout.WEST);
		
		//--Etiquette affichant l'image
		JLabel img  = new JLabel(new ImageIcon("Ressources/Images/imgPendu.JPG"));
		this.add(img, BorderLayout.EAST);
	}
	
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * M�thode de tri decroissant
	 * @return
	 */
	public CopyOnWriteArrayList<Joueur1> triDecroissant() {
		Comparator<Joueur1> comparator = (x, y) -> (x.getScore() < y.getScore()) ? 1 : ((x.getScore() == y.getScore()) ? 0 : -1); // ordre d�croissant
		this.listJoueur.sort(comparator);
		return this.listJoueur;
	}

}
