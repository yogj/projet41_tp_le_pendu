package le_pendu_page_ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Accueil extends JPanel {

	private static final long serialVersionUID = 1L;
	private String nom = "Accueil";

	/**
	 * Constructeur de la page d'accueil
	 */
	public Accueil() {
		//--on param le rangement des comp et la couleur de fond
		this.setLayout(new BorderLayout());
		this.setBackground(Color.WHITE);
				
		//--Etiquette affichant le titre
		JLabel titre = new JLabel ("Bienvenue dans le jeu du PENDU !");
		Font policeTitre = new Font("Arial", Font.BOLD, 25);
		titre.setFont(policeTitre);
		titre.setPreferredSize(new Dimension(300,100));
		titre.setForeground(Color.BLACK);
		titre.setHorizontalAlignment(JLabel.CENTER);
		this.add(titre, BorderLayout.NORTH);
		
		//--Etiquette affichant l'image
		JLabel img = new JLabel(new ImageIcon("Ressources/Images/imgComplete.JPG"));
		this.add(img, BorderLayout.CENTER);
		
		//--Zone de texte
		JTextArea message = new JTextArea();
		Font police = new Font("Arial", Font.BOLD, 18);
		message.setFont(police);
		message.setPreferredSize(new Dimension(760,75));
		message.setForeground(Color.BLACK);
		message.setEditable(false);
		message.setText("Vous avez 7 coups pour trouver le mot cach� ! Et si vous r�ussissez, on recommence ! \n"
				  		+ "\n Plus vous avez trouv� de mots, plus votre score grossit ! A vous de jouer !");
		this.add(message, BorderLayout.SOUTH);
	}
	/**
	 * Methode retournant le nom de la page
	 * @return nom
	 */
	public String getNom() {
		return this.nom;
	}
}
