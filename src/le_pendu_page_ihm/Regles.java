package le_pendu_page_ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Regles extends JPanel {

	private static final long serialVersionUID = 1L;
	private String nom = "Regles";

	
	public Regles() {
		//--on param le rangement des comp et la couleur de fond
		this.setLayout(new BorderLayout());
		this.setBackground(Color.WHITE);
		
		//--Etiquette affichant le titre
		JLabel titre = new JLabel("Regles du jeu du PENDU !");
		Font policetitre = new Font("Arial", Font.BOLD, 25);
		titre.setFont(policetitre);
		titre.setPreferredSize(new Dimension(500,50));
		titre.setForeground(Color.BLACK);
		titre.setHorizontalAlignment(JLabel.CENTER);
			
		this.add(titre, BorderLayout.NORTH);
		
		//--Zone de texte affichant les regles
		String messageRegle = " Vous avez 7 coups pour trouver le mot cach� ! Et si vous r�ussissez, on recommence ! \n";
		       messageRegle += "\n Plus vous avez trouv� de mots, plus votre score grossit ! A vous de jouer !\n\n\n"
		       				+ " COMPTES DES POINTS : \n"
		       				+ " ****************************** \n"
		       				+ " Mot trouv� sans erreur      : .....................100 points\n"
		       				+ " Mot trouv� avec 1 erreur   : ...................... 50 points\n"
		       				+ " Mot trouv� avec 2 erreurs : ...................... 35 points\n"
		       				+ " Mot trouv� avec 3 erreurs : ...................... 25 points\n"
		       				+ " Mot trouv� avec 4 erreurs : ...................... 15 points\n"
		       				+ " Mot trouv� avec 5 erreurs : ...................... 10 points\n"
		       				+ " Mot trouv� avec 6 erreurs : ........................ 5 points \n\n\n"
		       				+ " Je vous souhaite bien du plaisir ......\n"
		       				+ " Et, si vous pensez pouvoir trouver le mot du premier coup, c'est que vous pensez"
		       				+ "\n que le dictionnaire est petit ! \n"
		       				+ " Hors, pour votre information, il contient plus de 336 000 mots.....Bonne chance !";
		Font police = new Font("Arial", Font.BOLD, 18);
		JTextArea message = new JTextArea(messageRegle);
		message.setFont(police);
		message.setPreferredSize(new Dimension(750,150));
		message.setForeground(Color.BLACK);
		message.setEditable(false);
		message.setText(messageRegle);
		
		this.add(message, BorderLayout.CENTER);
	}
	/**
	 * Methode retournant le nom de la page
	 * @return nom
	 */
	public String getNom() {
		return this.nom;
	}

}
