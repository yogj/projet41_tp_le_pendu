package le_pendu_clavier;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.KeyStroke;



public class Clavier extends JPanel implements Observable{
	
	//--variables d'instance
	private static final long serialVersionUID = 1L;
	private String[] listChar = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
	private JButton[] listBouton = new JButton[this.listChar.length];
	private JPanel panBouton = new JPanel();
	private GridLayout gl = new GridLayout(5,6);
	private String lettre  ;
	private char charLettre;
	private ArrayList<Observateur> listObs = new ArrayList<Observateur>();
	
	/**
	 * Constructeur du clavier sans parametres
	 */
	public Clavier() {
		//--Le panel qui accueillera les boutons selon un rangement sur grille gl
		gl.setHgap(7);
		gl.setVgap(7);
		panBouton.setLayout(gl);
		//panBouton.addKeyListener(new ClavierListener());

		Font police = new Font("Arial", Font.BOLD, 23);
		//--On construit nos boutons dans la liste
		for(int i = 0; i <listChar.length; i++) {
			//--on cree nos boutons avec des ecouteurs clavier integres
			listBouton[i]=(createBouton(this.listChar[i], 65+i));
			listBouton[i].setFont(police);
			listBouton[i].setPreferredSize(new Dimension(55,55));
			//--On ajoute les ecouteurs souris sur les boutons, les ecouteurs claviers sont inclus ds les boutons
			listBouton[i].addActionListener(new ClavierListener());
			listBouton[i].setEnabled(true);
		
			//--On ajoute les boutons au panel
			panBouton.add(listBouton[i]);
		}
		//--On ajoute le panel � l'objet
		this.add(panBouton);
	}
	/**
	 * Encapsulation de la lettre
	 * @return un char = lettre tapee
	 */
	public char getLettre() {
		return this.charLettre;
	}

	/**
	 * Pattern Observer; methode d'ajout des abonnes
	 */
	public void addObservateur(Observateur o) {
		this.listObs.add(o);
		
	}

	/**
	 * Pattern Observer; mise a jour des abonnes
	 */
	public void updateObservateur() {
		for (Observateur o : listObs) {
			o.update(this.charLettre);
		}
		
	}

	/**
	 * Pattern Observer; suppression d'abonnes
	 */
	public void delObservateur() {
		this.listObs = new ArrayList<Observateur>();
		
	}
	/**
	 * M�thode g�n�rant un boutons qui bind une touche du clavier
	 * @param name
	 * @param virtualKey
	 * @return
	 */
	public JButton createBouton(String name, int virtualKey) {
        JButton btn = new JButton(name);
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println(e.getActionCommand() + " was clicked");
            }
        });
        InputMap im = btn.getInputMap(WHEN_IN_FOCUSED_WINDOW);
        ActionMap am = btn.getActionMap();
        im.put(KeyStroke.getKeyStroke(virtualKey, 0), "clickMe");
        am.put("clickMe", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JButton btn = (JButton) e.getSource();
                btn.doClick();
            }
        });
        return btn;
    }
	
	/**
	 * Classe interne = ecouteur du clavier
	 * @author nicolas
	 *
	 */
	class ClavierListener implements ActionListener {
		/**
		 * methode retournant la lettre lorsqu'on click dessus
		 */
		public void actionPerformed(ActionEvent e) {
			lettre = ((JButton)e.getSource()).getText();
			charLettre = lettre.charAt(0);
			updateObservateur();
			((JButton)e.getSource()).setEnabled(false);
		}

	}
}
