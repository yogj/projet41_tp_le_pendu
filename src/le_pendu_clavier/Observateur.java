package le_pendu_clavier;

/**
 * Interface Observateur du pattern Observer
 * avec la methode qui met a jour la lettre observee
 * @author nicolas
 *
 */
public interface Observateur {
	public void update(char pLettre);
}
