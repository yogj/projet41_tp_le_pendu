package le_pendu_main;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Comparator;
import java.util.concurrent.CopyOnWriteArrayList;

import le_pendu_joueur.Joueur1;

/**
 * Classe def l'objet qui gere les flux
 * @author nicolas
 *
 */

public class JoueurSerializer {

	private File fileScore;
	private CopyOnWriteArrayList<Joueur1> listJoueur;
	int compteurIndex = 0;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	
	public JoueurSerializer() {
		this.fileScore = new File("Ressources/Fichiers/joueurScore.txt");
		this.listJoueur = new CopyOnWriteArrayList<Joueur1>();
	}
	
	public JoueurSerializer(Joueur1 pJoueur) {
		this.fileScore = new File("Ressources/Fichiers/joueurScore.txt");
		this.listJoueur = new CopyOnWriteArrayList<Joueur1>();
	}
	
	/**
	 * M�thode g�rant la sauvegarde
	 */
	public void sauvegarderFile() {
			//--On trie la liste
			this.triDecroissant();
			//--si le fichier n'existe pas, on le cr�e vide
			if (! fileScore.exists()) {
				try {
                    fileScore.createNewFile();
 				}catch (IOException e) {
                    e.printStackTrace();
				}
			}
			//--sinon, on svg notre liste de joueur
			else {
				try {
					oos = new ObjectOutputStream(
							new BufferedOutputStream(
									new FileOutputStream(fileScore)));
					oos.writeObject(listJoueur);
					System.out.println("Taille du fichier = " + fileScore.length()); //Controle
					System.out.println("listJoueur svg : \n"+this.toString());//controle
					oos.close();
				}catch (FileNotFoundException e) {
					e.printStackTrace();
				}catch (IOException e) {
					e.printStackTrace();				
				}
			}
	}
	
	/**
	 * Methode gerant la lecture du fichier
	 */
	public void lireFile() {
		//--Si le fichier existe, on verifie s'il n'est pas vide auquel cas on le lit et on cree le contenu de notre listJoueur
				try {
					if (fileScore.exists()) {
						if (fileScore.length() !=0) {
							try {
								ois = new ObjectInputStream(
										new BufferedInputStream(
												new FileInputStream(fileScore)));
								this.listJoueur= (CopyOnWriteArrayList<Joueur1>)ois.readObject();
								System.out.println("list joueur recup sur le fichier lors de lecture : \n"+this.toString());//Controle
								ois.close();
							}catch (ClassNotFoundException e){
								e.printStackTrace();					
							}
						}
					}
					//--Sinon, on le cree
					else {
						try {
		                    fileScore.createNewFile();
		 				}catch (IOException e) {
		                    e.printStackTrace();
						}
					}
				}catch (FileNotFoundException e) {
					e.printStackTrace();
				}catch (IOException e) {
					e.printStackTrace();
				}
	}
	
	/**
	 * Methode renvoyant la liste de joueur
	 */
	public CopyOnWriteArrayList<Joueur1> getListe(){
		return this.listJoueur;
	}
	
	/**
	 * Methode de descr de la liste de joueur (Controle)
	 */
	public String toString() {
		String str = "";
		for (Joueur1 j : this.listJoueur)
			str += " Joueur : "+j.getNom()+ " - score : "+j.getScore()+" nb mot : "+j.getNbMot()+"\n";
		return str ;
	}
	/**
	 * M�thode de tri decroissant
	 * @return
	 */
	public CopyOnWriteArrayList<Joueur1> triDecroissant() {
		Comparator<Joueur1> comparator = (x, y) -> (x.getScore() < y.getScore()) ? 1 : ((x.getScore() == y.getScore()) ? 0 : -1); // ordre d�croissant
		this.listJoueur.sort(comparator);
		return this.listJoueur;
	}
	
	/**
	 * M�thode qui v�rifie si le joueur est deja sur la liste, recup son score et le supprime de la liste le cas echeant puis l'ajoute
	 * a la liste avec son score actualise
	 * @return
	 */
	public void checkJoueur(Joueur1 pJ){
		//--Pour chaque joueur de la liste on v�rif le nom du joueur que l'on compare au nom de notre joueur et on compare son score
		for(Joueur1 j : this.listJoueur) {
			if(pJ.getNom().equals(j.getNom())){ 
				if(pJ.getScore() != j.getScore()) {//--Si le score est diff�rent entre le fichier et l'appli, on le met a jour
					System.out.println(" notre joueur : "+ pJ.getNom()+" son score actuel : "+pJ.getScore());//Controle
					pJ.setScore((pJ.getScore())+j.getScore());//Actualisation du score
					pJ.setNbMot((pJ.getNbMot())+j.getNbMot());//Actualisation du nb de mot
					System.out.println(" notre joueur : "+pJ.getNom()+" - score actualise : "+pJ.getScore()+"\n"
								 + " le joueur de la liste : "+j.getNom()+" - score : "+j.getScore());//Controle
					this.listJoueur.remove(j);
				}
				else {//--Sinon on n'actualise pas le score
					System.out.println(" notre joueur-> : "+ pJ.getNom()+" son score actuel : "+pJ.getScore());//Controle
					pJ.setScore((pJ.getScore()));//Actualisation du score
					pJ.setNbMot((pJ.getNbMot()));//Actualisation du nb de mot
					System.out.println(" notre joueur-> : "+pJ.getNom()+" - score actualise : "+pJ.getScore()+"\n"
								 + " le joueur de la liste : "+j.getNom()+" - score : "+j.getScore());//Controle
					this.listJoueur.remove(j);
				}
					
			}
		}
		//--On ajoute ensuite notre joueur
		this.listJoueur.add(pJ);
	}
	
}
