package le_pendu_main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import le_pendu_joueur.Joueur1;
import le_pendu_page_ihm.Accueil;
import le_pendu_page_ihm.Regles;
import le_pendu_page_ihm.Scores;
import le_pendu_page_ihm.TableDeJeu;

public class Le_Pendu extends JFrame{
	private TableDeJeu jeu = new TableDeJeu();
	private Regles regle = new Regles();
	private Accueil accueil = new Accueil();
	private Scores score;
	private JoueurSerializer gestionFile;
	private CopyOnWriteArrayList<Joueur1> listeJoueur = new CopyOnWriteArrayList<Joueur1>();
	private static final long serialVersionUID = 1L;

	//--attributs pour la barre de menu
	private JMenuBar jmb = new JMenuBar();
	private JMenu fichier = new JMenu("Fichier"),
				  aPropos = new JMenu("A Propos");
	private JMenuItem nouveauMenu = new JMenuItem("Nouveau", 'n'),
					  scoreMenu = new JMenuItem("Score", 's'),
					  regleMenu = new JMenuItem("Regles", 'r'),
					  quitter = new JMenuItem("Quitter", 'q'),
					  aProposMenu = new JMenuItem("A Propos",'a');
	
	/**
	 * Constructeur sans param
	 */
	public Le_Pendu() {
		//--Param�tres de la fenetre
		this.setTitle("LE PENDU");
		this.setForeground(Color.BLACK);
		this.setBackground(Color.WHITE);
		this.setSize(new Dimension(800,650));
		this.setResizable(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		

	
		
				
		//--Initialisation du menu
		this.initMenu();
		
		//--Affichage de la page d'accueil
		this.afficher(accueil.getNom());

		//--Ajout d'un listener : au clic on propose une nvelle partie
		this.addMouseListener(new MouseListener() {
			public void mouseClicked(MouseEvent e) {
				nouveauMenu.doClick();
			}

			@Override
			public void mouseEntered(MouseEvent e) {}

			@Override
			public void mouseExited(MouseEvent e) {}

			@Override
			public void mousePressed(MouseEvent e) {}

			@Override
			public void mouseReleased(MouseEvent e) {}
		});
		
	}
	/**
	 * Methode construisant la barre de menu et ses sous menus
	 */
	public void initMenu() {
		//--Menu Fichier
		fichier.setMnemonic('F');
		
		//--Nouveau
		nouveauMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK ));
		nouveauMenu.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				//--Si une partie est en cours
				if (jeu.getPartieLancee()) {
					JOptionPane jop = new JOptionPane();
					jop.showMessageDialog(null, "Vous �tes deja dans une partie !",
										"Attention", 
										JOptionPane.WARNING_MESSAGE);
				}
				else {
					afficher(jeu.getNom());
					jeu.nouvellePartie();
					jeu.setPartieLancee(true);
				}
			}
		});
		fichier.add(nouveauMenu);
		fichier.addSeparator();
		
		//--Score
		//--Lecture du fichier joueurScore, maj du joueur et de la liste
		
		scoreMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK ));
		scoreMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gestionFile = new JoueurSerializer();
				gestionFile.lireFile();
				gestionFile.checkJoueur(jeu.getJoueur());
				gestionFile.sauvegarderFile();
				listeJoueur = gestionFile.getListe();
				score = new Scores(listeJoueur);
				//--Si une partie est en cours
				if(jeu.getPartieLancee()) {
					JOptionPane jop = new JOptionPane();
					int option = jop.showConfirmDialog(null, "Vous �tes deja dans une partie !\n Voulez-vous quitter ?",
													"Attention", 
													JOptionPane.YES_NO_CANCEL_OPTION,
													JOptionPane.QUESTION_MESSAGE);
					if (option == JOptionPane.OK_OPTION) {
						jeu.setPartieLancee(false);
						afficher(score.getNom());
					}
				}
				else {
					afficher(score.getNom());
					}
			}
		});
		fichier.add(scoreMenu);
		fichier.addSeparator();
		
		//--Regles
		regleMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, KeyEvent.CTRL_DOWN_MASK ));
		regleMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(jeu.getPartieLancee()) {
					JOptionPane jop = new JOptionPane();
					int option = jop.showConfirmDialog(null, "Vous �tes deja dans une partie !\n Voulez-vous quitter ?",
													"Attention", 
													JOptionPane.YES_NO_CANCEL_OPTION,
													JOptionPane.QUESTION_MESSAGE);
					if (option == JOptionPane.OK_OPTION) {
						jeu.setPartieLancee(false);
						afficher(regle.getNom());
					}
				}
				else
					afficher(regle.getNom());
			}
		});
		fichier.add(regleMenu);
		fichier.addSeparator();
		
		//--Quitter
		quitter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_DOWN_MASK ));
		quitter.addActionListener(new QuitterListener()); 

		fichier.add(quitter);
		
		//--Menu a propos
		aPropos.setMnemonic('A');
		
		//--a propos
		aProposMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.CTRL_DOWN_MASK ));
		aProposMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//je sais pas ce qu'il y a derriere ce menu 
				JOptionPane jop = new JOptionPane();
				String message = "Appli du Pendu. Pas de copyright !\n";
				message += "L'auteur nie toute responsabilit� quant au fonctionnement de l'appli. \n";
				message += "Ceci est un TP, ca tourne tout seul !";
				jop.showMessageDialog(null, message, "A propos", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		aPropos.add(aProposMenu);
		
		//--Ajout des menus fichier et apropos ds la barre de menu
		jmb.add(fichier);
		jmb.add(aPropos);
		
		this.setJMenuBar(jmb);
	}

	/**
	 * Methode pour afficher les differentes pages selon leur nom
	 * @param pNomPage
	 */
	public void afficher(String pNomPage) {
		this.getContentPane().removeAll();
		switch(pNomPage) {
			case("Accueil") : this.getContentPane().add(accueil);break;
			case("Regles") : this.getContentPane().add(regle); break;
			case("Scores") : this.getContentPane().add(score);break;
			case("Table De Jeu") : this.getContentPane().add(jeu);break;
		}
		this.getContentPane().revalidate();
		this.getContentPane().repaint();
	}
	

	/**
	 * classe interne def le comportement de "Quitter"
	 * @author nicolas
	 *
	 */
	class QuitterListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			JOptionPane jop = new JOptionPane();
			int option = jop.showConfirmDialog(null, "Voulez-vous quitter ?",
							"Quitter", 
							JOptionPane.YES_NO_CANCEL_OPTION,
							JOptionPane.QUESTION_MESSAGE);
			if (option == JOptionPane.OK_OPTION) {
				gestionFile.lireFile();
				gestionFile.checkJoueur(jeu.getJoueur());
				gestionFile.sauvegarderFile();
				System.exit(0);
			}
		}
	}

		
	public static void main(String[] args) {
		Le_Pendu f = new Le_Pendu();
		f.setVisible(true);

	}

}
